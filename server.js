﻿// Déclarations des modules
const express = require('express')
const path = require('path');
const fs = require('fs')

// BDD et POST init
var MongoClient = require("mongodb").MongoClient;
var bodyParser = require('body-parser');

// Déclaration du serveur
const hostname = '127.0.0.1';
const port = 3000;

// Repertoire
const commons = path.join(__dirname, 'datas');

// Lancement de express
var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


// Génération du HTML de sortie
function generateHTML(headers, body){
  var html = '<!DOCTYPE HTML>\n\t<head>\n';
  headers.forEach(balise=>html += '\t\t' + balise + '\n');
  html += '\t</head>\n\t<body>';
  body.forEach(balise=>html += '\t\t' + balise + '\n');
  html += '\t</body>\n</html>';
  return html;
}

// Traitement des demandes
app.route(['/datas', '/datas/*']).get((req, res, next)=>{
  if(!req.params || !req.params[0]){
    req.dest = commons;
  }else{
    req.dest = path.join(commons, req.params[0]);
  }
  
  fs.exists(req.dest, exists=>{
    if(exists){
      next();
    }else{
      res.status(404).end();
    }
  });
}, (req, res, next)=>{
  fs.lstat(req.dest, (err, stats)=>{
    if(stats.isDirectory()){
      next();
    }else{
      res.sendFile(path.join(commons, req.params[0]));
    }
  });
}, (req, res)=>{
  fs.readdir(req.dest, (err, files)=>{html
    if(err){
      res.status(500).end();
      return;
    }
    var format = {
      text: function(){
        res.setHeader('Content-Type', 'text/plain; charset=utf-8')
        .send(files.toString());
      },
      json: function(){
        res.json(files);
      },
      html: function(){
        res.setHeader('Content-Type', 'text/html; charset=utf-8');
        var headers = [
          '<meta http-equiv="content-type" content="text/html; />',
          '<title>' + req.title + '</title>'
        ];
        var list = files.map(file=>'<li><a href="' + req.path + '/' + file + '">' + file + '</a></li>');
        var up = req.path.substr(0, req.path.lastIndexOf('/'));
        if(up)
          list.unshift('<li><a href="' + up + '">..</a></li>');
          list.unshift('<ul>');
          list.push('</ul>');
          res.send(generateHTML(headers, list));
      }
    };
    if(req.query && req.query.format){
      format[req.query.format]();
    }else{
      res.format(format);
    }
  });
});


// Post pour BDD
app.post('/bdd/test', function(req, res){
  console.log(req.body)
  var user_id = req.body.id;
  var token = req.body.token;

  var test = 'user_id = ' + user_id + 'et token = '+ token
  console.log(test)  
  res.send(test)
});


// Appel à la BDD - 	DO NOT WORK
//MongoClient.connect("mongodb://localhost/tutoriel",function(error,db){
//  if(error)
//    return funcCallback(error);
//  console.log('Connecté à la bdd');
//});


app.listen(port);
