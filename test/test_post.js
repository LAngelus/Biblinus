// déclaration
var querystring = require('querystring');
var http = require('http');
var fs = require('fs');

const hostname = '127.0.0.1';
const port = 3000;

// Variable de test
  var post_data = querystring.stringify({
      'user_id' : '28480',
      'token': 'kjzkjznzzkjz45454z',
  });

   
  // configuration de l'envoi
  var post_options = {
      host: hostname,
      port: port,
      path: '/bdd/test',
      method: 'POST',
      headers: {
          'Content-Type': 'application/json',
          'Content-Length': Buffer.byteLength(post_data)
      }
  };

  // Set up the request
  var post_req = http.request(post_options, function(res) {
      res.setEncoding('utf8');
      res.on('data', function (chunk) {
          console.log('Response: ' + chunk);
      });
  });

  // post the data
  post_req.write(post_data);
  post_req.end();
