var http = require('http');
var querystring = require('querystring');
var fs = require('fs');
var request_options = require('./request_options.json');
var request_datas = querystring.stringify(require('./request_datas.json'));

fs.exists('./result.txt', (a, e)=>{
	if(e) fs.rmdir('./result.txt');
});

request_options.headers = {
  'Content-Type': 'application/x-www-form-urlencoded',
  'Content-Length': Buffer.byteLength(request_datas)
};

var post = http.request(request_options, (res)=>{
	res.setEncoding('utf8');
	res.on('data', (chunk)=>{
		if(chunk)
			fs.appendFile('./result.txt', chunk, 'utf8');
	});
});

post.write(request_datas);
post.end();
